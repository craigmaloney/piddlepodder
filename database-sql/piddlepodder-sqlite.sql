create table shows (
id integer primary key,
title varchar(255),
url varchar(255),
directory varchar(255),
active boolean);

create table queue (
id integer primary key,
show_id integer
    constraint fk_show_id references shows(id) on delete cascade,
url varchar(255),
downloaded boolean,
timestamp date);
