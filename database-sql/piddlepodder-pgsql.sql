--
-- PostgreSQL database dump
--

SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: queue; Type: TABLE; Schema: public; Owner: piddlepodder; Tablespace: 
--

CREATE TABLE queue (
    id integer NOT NULL,
    show_id integer,
    url character varying(255) DEFAULT NULL::character varying,
    downloaded integer,
    "timestamp" timestamp without time zone
);


ALTER TABLE public.queue OWNER TO piddlepodder;

--
-- Name: queue_id_seq; Type: SEQUENCE; Schema: public; Owner: piddlepodder
--

CREATE SEQUENCE queue_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.queue_id_seq OWNER TO piddlepodder;

--
-- Name: queue_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: piddlepodder
--

ALTER SEQUENCE queue_id_seq OWNED BY queue.id;


--
-- Name: shows; Type: TABLE; Schema: public; Owner: piddlepodder; Tablespace: 
--

CREATE TABLE shows (
    id integer NOT NULL,
    title character varying(255) DEFAULT NULL::character varying,
    url character varying(255) DEFAULT NULL::character varying,
    directory character varying(255) DEFAULT NULL::character varying,
    active integer
);


ALTER TABLE public.shows OWNER TO piddlepodder;

--
-- Name: shows_id_seq; Type: SEQUENCE; Schema: public; Owner: piddlepodder
--

CREATE SEQUENCE shows_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.shows_id_seq OWNER TO piddlepodder;

--
-- Name: shows_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: piddlepodder
--

ALTER SEQUENCE shows_id_seq OWNED BY shows.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: piddlepodder
--

ALTER TABLE queue ALTER COLUMN id SET DEFAULT nextval('queue_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: piddlepodder
--

ALTER TABLE shows ALTER COLUMN id SET DEFAULT nextval('shows_id_seq'::regclass);


--
-- Name: queue_pkey; Type: CONSTRAINT; Schema: public; Owner: piddlepodder; Tablespace: 
--

ALTER TABLE ONLY queue
    ADD CONSTRAINT queue_pkey PRIMARY KEY (id);


--
-- Name: shows_pkey; Type: CONSTRAINT; Schema: public; Owner: piddlepodder; Tablespace: 
--

ALTER TABLE ONLY shows
    ADD CONSTRAINT shows_pkey PRIMARY KEY (id);


--
-- Name: queue_show_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: piddlepodder
--

ALTER TABLE ONLY queue
    ADD CONSTRAINT queue_show_id_fkey FOREIGN KEY (show_id) REFERENCES shows(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

