#!/usr/bin/perl
# PiddlePodder Podcatching client
# Written By Craig Maloney
# (c) 2005-2010 Craig Maloney
# Released under the GPL
use strict;
use warnings;

use DBI;
use Getopt::Long;
use LWP::Simple;
use LWP::UserAgent;
use Time::localtime;
use XML::DOM;

my $debug = 0;
my $pp_version = "1.1";
my $date = &get_date;
my %config;

&load_defaults;

my (
    $activate,
    $add,
    $catchup,
    $deactivate,
    $delete,
    $display_feeds,
    $dont_download,
    $list,
    $usage,
);

GetOptions (
    "help|h" => \$usage,
    "activate=s" => \$activate,
	"add|a" => \$add,
	"catchup|c" => \$catchup,
    "delete=s" => \$delete,
    "deactivate=s" => \$deactivate,
	"display|d" => \$display_feeds,
    "list|l" => \$list,
	"nodownload|x" => \$dont_download,
	"debug+" => \$debug
);

&display_usage if ($usage);

# If two debugs are present, turn display feeds on automatically
$display_feeds = 1 if ($debug > 2);

my %queries = (
    'active_shows' => 'select id, title, url, directory from shows where active = 1',
    'active' => 'update shows set active = ? where id = ?',
    'add_show' => 'insert into shows (url,title,directory,active) values (?,?,?,1)',
    'catchup' => 'update queue set downloaded = 1',
    'delete_queue' => 'delete from queue where show_id = ?',
    'delete_show' => 'delete from shows where id = ?',
    'download_active' => 'select id, url from queue where downloaded = 0 and show_id in (select id from shows where active = 1)', 
    'enqueue' => 'insert into queue (show_id, url, downloaded, timestamp) values (?,?,?,?)',
    'list_shows' => 'select id, title, url, directory, active from shows order by active desc, id asc',
    'queue_done' => 'update queue set downloaded = 1 where id = ?',
    'queued' => 'select id from queue where url = ?',
    'queue_incomplete' =>'select id, url from queue where downloaded = 0',
    'show_title_by_id' => 'select title from shows where id = ?',
    'unique_url' => 'select id from shows where url like ?',
);

if (!-e $config{'config_dir'}) {
	&create_configuration_dir;
}

if (!-e $config{'config_file'}) {
	&create_rc_file;
	exit(1);
}

&load_config_file($config{'config_file'});

print $config{'db_host'} if $debug;
my $dbh = DBI->connect($config{'db_host'},$config{'db_user'},$config{'db_password'},{RaiseError => 1,AutoCommit => 1}) or die "Whoops.";

if (!-e $config{'download_dir'}) {
	die "Download directory $config{'download_dir'} does not exist";
}

my $download_location = $config{'download_dir'} . "/$date"; # Directory to download files

mkdir "$download_location" || die "Can't create destination directory $download_location \nPlease create it manually.\n";
chdir "$download_location";

# Handle a rudimentary interface
if ($add) {
    my ($add_url, $add_title, $add_directory) = @ARGV;
    &add_show($add_url, $add_title, $add_directory);
}
&list_shows if ($list);
&delete_show($delete) if ($delete);

# Handle show activity
&set_activity($activate, 1) if ($activate);
&set_activity($deactivate, 0) if ($deactivate);

# Initialize...
my $exceptions_regex = '';
&load_exceptions;
&find_enclosures($dbh);
&display_enclosures if ($display_feeds);
die "Refusing to download files by request\n" if ($dont_download);
&download_enclosures($dbh);
exit(0);


sub find_enclosures {
    my $dbh = shift;
	my $subscription;
    my $id;
    my $url;
    my $title;
    my $directory;

# This section comes from PerlPodder by sam.versluys@gmail.com 
	my $LWP_USER_AGENT = LWP::UserAgent->new;
	$LWP_USER_AGENT->env_proxy;
	$LWP_USER_AGENT->agent("PiddlePodder/$pp_version");

    my $sth = $dbh->prepare($queries{'active_shows'});
    $sth->execute();
    while ((my $show_id, my $title, my $url, my $directory) = $sth->fetchrow_array) { 
		eval {
			print "\nparsing $title ($url)\n" if ($debug>1);

			my $parser = new XML::DOM::Parser ($LWP_USER_AGENT);
			my $doc = $parser->parsefile($url);

			foreach my $item ($doc->getElementsByTagName('item')) {

				eval {
					my $enclosure = $item->getElementsByTagName('enclosure')->item(0);
					if ($enclosure) {
						my $enclurl = $enclosure->getAttributeNode('url')->getNodeValue;
						# my $encllength = $enclosure->getAttributeNode ("length")->getNodeValue;
						&add_enclosure_url($dbh,$show_id,$enclurl,$directory);
					}
				}
			}
		};
		if ($@) {
			warn "$title has screwed up their XML\n$@\n";
		} 
	}
}

sub display_enclosures {

    my ($id, $url);
	print "Downloading the following files:\n";
    my $sth = $dbh->prepare($queries{'queue_incomplete'});
    $sth->execute();

    while (($id,$url) = $sth->fetchrow_array()) {
		print "$url\n";
	}
}

sub download_enclosures {
    my $dbh = shift;
    my $sth = $dbh->prepare($queries{'download_active'});
    my ($url,$id);
    $sth->execute();
    
    if ($catchup) {
        $sth = $dbh->prepare($queries{'catchup'});
        $sth->execute();
        return;
    }

    while ((my $id, $url) = $sth->fetchrow_array()) {
		print "Retreiving $url\n" if ($debug>0);
		`$config{'download_program'} -a $config{'log_file'} -c $url`;
		if ($? != 0) {
			warn "Download of $url failed. Please check the logs for more information\n";
		} else {
			&add_completed($dbh,$id);
		}
	}
}

sub add_completed {
    my ($dbh, $id) = @_;
    print "Marking queue $id as done\n" if ($debug > 0);
    my $sth = $dbh->prepare($queries{'queue_done'});
    $sth->execute($id) or die "Can't mark queue $id done";
}

sub create_configuration_dir {
	print "Creating default configuration directory...\n";
	mkdir $config{'config_dir'};
	print "Done.\n";
}

sub create_rc_file {
	print "Creating default configuration file...\n";
	open FILE, ">$config{'config_file'}";
	print FILE "# PiddlePodder Configuration File\n";
	print FILE "# Version: $pp_version\n\n";
    foreach my $keys (sort(keys %config)) {
        print FILE $keys,"\t\t",$config{$keys},"\n";
    }
	close FILE;
	print "Done.\n";
	print "(Please check the configuration of $config{'config_file'} \nbefore running this program again).\n";
}

sub is_queued {
    my $dbh = shift;
    my $enclurl = shift;
    my $found = undef;
    my $sth = $dbh->prepare($queries{'queued'});
    $sth->execute($enclurl);
    $found = $sth->fetch();
    if ($found) {
        print " $enclurl already queued\n" if ($debug > 0);
        return 1;
    } else {
        print " $enclurl is not queued\n" if ($debug > 0);
        return 0;
    }
}

sub load_exceptions {
	my @exceptions;
	if (-e $config{'list_of_exceptions'}) {
		open EXCEPTIONS, $config{'list_of_exceptions'}; 
		while (<EXCEPTIONS>) {
			chomp;
			push @exceptions, $_;
		}
		close EXCEPTIONS;
	}
	if (scalar(@exceptions) > 0) {
		$exceptions_regex = join '|', @exceptions;
	}
}

sub is_exception {
	my $url = shift;
	if ($exceptions_regex eq '') { return 0 };
	if ($url =~ /.*(?:$exceptions_regex).*/i) {
		print "Matched exception: $url\n" if ($debug>0);
		return 1;
	} else {
		return 0;
	}
}

sub add_enclosure_url {
    my ($dbh, $id, $url, $dir) = @_;
	if ( grep /^http/, $url ) {
		print "$url\n" if ($debug>2);
		if (!&is_queued($dbh,$url)) {
			if (!&is_exception($url)) {
                my $sth = $dbh->prepare($queries{'enqueue'});
                $sth->execute($id, $url, 0,$date) or die "Can't insert into database";
				print "Scheduled for download: $url\n" if ($debug>0);
			}
		}
	}
}


sub load_config_file() {
	my $config_file = shift;
    my $cmd; 
    my $rest;

	open (CONFIG,"$config_file") or die ("Can't find config file in $config_file");

# These come out of the program 'aub'. 

	foreach (<CONFIG>) {
		chop;   
		# Remove comments
		s/^#.*$//;
		s/[^\\]#.*$//;
		# Unescape escaped comments
		s/\\#/#/g;
		# Skip blank lines
		next if m/^\s*$/;
		# Fold white space into 1 <SP>
		s/\s+/ /;
		# Drop leading and trailing white space 
		s/^ //;
		s/ $//;

		($cmd, $rest) = split(/\s+/, $_, 2);
		&parse($cmd, $rest);
	}
}

sub parse {
	my ($cmd, $args) = @_;

    $config{lc($cmd)} = $args;
    return;
}

sub load_defaults {
	my $home_dir = $ENV{HOME};

	$config{'config_dir'} = "$home_dir/.piddlepodder";
	$config{'config_file'} = "$home_dir/.piddlepodder/piddlepodderrc";
    $config{'db_host'} = 'dbi:Pg:dbname=piddlepodder;host=localhost';
    $config{'db_password'} = 'changeme';
    $config{'db_user'} = 'piddlepodder';
	$config{'download_dir'} = "$config{'config_dir'}";
	$config{'list_of_exceptions'} = "$config{'config_dir'}/exceptions";
	$config{'feed_list'} = "$config{'config_dir'}/feeds";
	$config{'log_file'} = "$config{'config_dir'}/wget.log";
	$config{'download_program'} = "/usr/bin/wget";
}

sub get_date {
    my $tm = localtime;
    my $date = sprintf("%04d-%02d-%02d", ($tm->year+1900), ($tm->mon)+1, $tm->mday);
    return $date;
}

sub add_show{
    my ($add_url, $add_title, $add_directory) = @_;

    # Give sane defaults if we don't already have them
    die "Missing URL. Exiting.\n" if (!defined($add_url));
    $add_title = '' if (!defined($add_title));
    $add_directory = '' if (!defined($add_directory));

    # Check for duplicate rows
    my $sth_select = $dbh->prepare($queries{'unique_url'});
    $sth_select->execute($add_url);
    my @duplicate_rows = $sth_select->fetchrow_array;
    die "Duplicate URL entered\n" if (@duplicate_rows);

    print (join "\n", $add_url, $add_title, $add_directory);
    my $sth = $dbh->prepare($queries{'add_show'});
    $sth->execute($add_url, $add_title, $add_directory) or die "Can't add show";
    print "Show added.\n";
    exit;
}

sub list_shows {

    my $sth = $dbh->prepare($queries{'list_shows'});
    $sth->execute();
    while (my @show_attributes = $sth->fetchrow_array) { 
        print join "\t", @show_attributes;
        print "\n"
    }
    exit;
}

sub delete_show {
    my $delete_id = shift;
    my $rv;
    print "\nDelete value: $delete_id\n" if $debug;

    my $show_title = &get_show_title($delete_id);

    $rv = $dbh->do($queries{'delete_queue'},undef,$delete_id) or die $dbh->errstr;
    print "$rv deleted\n" if ($debug && ($rv > 0));
    $rv = $dbh->do($queries{'delete_show'},undef,$delete_id) or die $dbh->errstr;
    print "$rv deleted\n" if ($debug && ($rv > 0));
    print "Show ID $delete_id ($show_title) deleted.\n";
    exit;
}

sub get_show_title {
    my $show_id = shift;

    my $sth = $dbh->prepare($queries{'show_title_by_id'});
    $sth->execute($show_id);
    my $show_title = $sth->fetchrow;
    if (defined $show_title) {
        return $show_title;
    } else  {
        return "No title given";
    }
}


sub set_activity {
    my $show_id = shift;
    my $active = shift;
    my $rv;
    print "\nUpdate value: $show_id with $active\n" if $debug;

    my $show_title = &get_show_title($show_id);

    $rv = $dbh->do($queries{'active'},undef,$active, $show_id) or die $dbh->errstr;
    print "Show ID $show_id ($show_title) updated to $active.\n";
    exit;
}


sub display_usage {
# FIXME Rudimentary. Needs to be cleaned up
    die <<EOF;
Usage: piddlepodder [OPTION]
Piddlepodder downloads podcasts from a list of RSS feeds

    -a, --add [url] [title] [dir]
                            Add feeds
    --activate [id]         Activate show ID
    -c, --catchup           Catch-up on feeds
    --deactivate [id]       Deactivate show ID
    --delete [id]           Delete a show by ID #
    -d, --display           Display list of enclosures to be downloaded
    -l, --list              List of shows, both active and inactive
    -x, --nodownload        Don't download files (only useful with -d)
    --debug [[--debug]...]  Display additional debug messages
        (where n is 1 for terse, 2 for verbose, 3 for excessive)
    -h, --help              Display this message
EOF
}
